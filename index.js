/**
 * Bài 1: Tính tiền lương nhân viên
 * Input:
 * Tiền lương 1 ngày của nhân viên: 100000
 * Số ngày làm: 30  
 * 
 * 
 * 
 * Step:
 * Step 1: Tạo 2 biến: tiền lương và số ngày làm
 * Step 2: Tạo biến chứa kết quả cần tìm
 * Step 3: Áp dụng công thức lương*số ngày làm
 * 
 * 
 * 
 * Output:3000000
 * 
 */
var salary=100000;
var daywork=30;

result=salary*daywork;
console.log('result: ', result);

/**
 * Bài 2: Tính giá trị trung bình
 * Input:
 * 5 chữ số ngẫu nhiên: 5,10,15,20,25
 * 
 * 
 * Step:
 * Step 1: Tạo 5 biến: num1=5, num2=10, num3=15, num4=20, num5=25
 * Step 2: Tạo biến chứa kết quả cần tìm
 * Step 3: Áp dụng công thức 5 số ở trên/5
 * 
 * Output:15
 */
var num1=5;
var num2=10;
var num3=15;
var num4=20;
var num5=25;

result=(num1+num2+num3+num4+num5)/5
console.log('result: ', result);

/**
 * Bài 3:Quy đổi tiền
 * Input:
 * Giá đô USD hiện nay 23500VND
 * Đổi ngẫu nhiên 2 đô
 * 
 * 
 * Step:
 * Step 1: Tạo 2 biến: biến đô cần đổi, biến tỉ giá tiền đô đổi sang tiền VND
 * Step 2: Tạo biến chứa kết quả cần tìm
 * Step 3: Áp dụng công thức: đô cần đổi*tỉ giá VND
 * 
 * 
 * Output:47000
 */
var usd=2;
var vnd=23500;
result=(usd*vnd)
console.log('result: ', result);

/**
 * Bài 4: Tính diện tích, chu vi hình chữ nhật
 * Input:
 * Chiều dài edge1=4
 * Chiều rộng edge2=3
 * 
 * 
 * Step:
 * Step 1: Tạo 2 biến: edge1 và edge2
 * Step 2: Tạo biến chứa kết quả cần tìm
 * Step 3: Áp dụng công thức S=edge1*edge2; C=(edge1+edge2)*2
 * 
 * 
 * Output:
 * S=12; C=14
 */
var edge1=4;
var edge2=3;
S=edge1*edge2;
console.log('S: ', S);
C=(edge1+edge2)*2;
console.log('C: ', C);


/**
 * Bài 5: Tính tổng 2 ký số
 * Input: 
 * Số random: 13
 * 
 * 
 * Step:
 * Step 1: Tạo 3 biến, 1 biến cho sẵn số 13, 2 biến còn lại dựa trên số 13 để lấy ra hàng chục, hàng đơn vị
 * Step 2: Tạo biến chứa kết quả cần tìm
 * Step 3: Áp dụng công thức: tổng=số hàng đơn vị + số hàng chục
 * 
 * Output:
 * 14
 * 
 */
var num1=13;
var donvi=num1%10;//3
var chuc=Math.floor (num1/10)%10;

T=donvi+chuc
console.log('T: ', T);

